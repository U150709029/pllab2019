import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.Timestamp;
import java.sql.Date;
import java.util.PriorityQueue;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class proje extends JFrame implements ActionListener{

	public  JButton buton;
	static String input;
	public  JLabel label;
	public  JTextField text;
    public JPanel panel;
		
	
	public static void main(String[] args) throws Exception{
		
		
		Scanner scanner = new Scanner(System.in);
		System.out.print("enter the file name :");
		String file_name = scanner.nextLine();
		
		
		 // Creating an empty PriorityQueue 
        PriorityQueue<String> queue = new PriorityQueue<String>(); 
		String fileName = file_name;
		String line = null;
      
		 try {
	            // FileReader reads text files in the default encoding.
	            FileReader fileReader = new FileReader(fileName);

	            // Always wrap FileReader in BufferedReader.
	            BufferedReader bufferedReader = new BufferedReader(fileReader);

	            while((line = bufferedReader.readLine()) != null) {
	                System.out.println(line);
	             // Use add() method to add elements into the Queue 
	                queue.add(line); 
	            }   

	            // Always close files.
	            bufferedReader.close();         
	        }
	        catch(FileNotFoundException ex) {
	            System.out.println(
	                "Unable to open file '" + 
	                fileName + "'");                
	        }
        JFrame frame=new JFrame();
        frame.setSize(400, 130);
        frame.setLocation(400, 250);
        frame.setLayout(new FlowLayout());
        
        
        JPanel panel=new JPanel();
        frame.add(panel);
        
        JLabel label=new JLabel();
        panel.setSize(400, 130);
        panel.setBackground(Color.RED);
        panel.add(label);
        
        
        JTextField text=new JTextField(10);
        panel.add(text);
        
        
        JButton buton=new JButton("INSERT");
        panel.add(buton);
        buton.addActionListener(new ActionListener()
        		{
        	 public void actionPerformed(ActionEvent e){
        	    	if(e.getSource()==buton){
        	    		
        	    	   	input=text.getText();
        	    	   	long startTime = System.nanoTime(); 
        	    	    queue.add(input);
        	    	    long estimatedTime = System.nanoTime() - startTime;
        	    	    System.out.println(estimatedTime);
        	    	    JOptionPane.showMessageDialog(null,"�NSERT T�ME �S :"+estimatedTime +" nano seconds" );
     	    	   	         	    	   	
        	    	}
        	    	}
        		}
        		);
       
        JButton buton2=new JButton("SHOW");
        panel.add(buton2);
        buton2.addActionListener(new ActionListener()
        		{
        	 public void actionPerformed(ActionEvent e){
        	    	if(e.getSource()==buton2){
        	    	   	       	    	 
        	    	   	JOptionPane.showMessageDialog(null,queue );
        	    	
        	    	}
        	    	}
        		}
        		);
       
        
        JButton buton3=new JButton("DELETE");
        panel.add(buton3);
        buton3.addActionListener(new ActionListener()
		{
	 public void actionPerformed(ActionEvent e){
	    	if(e.getSource()==buton3){
	    		
	    	   	input=text.getText();
	    	   	long startTime = System.nanoTime(); 
	    	    queue.remove(input);
	    	    long estimatedTime = System.nanoTime() - startTime;
	    	    System.out.println(estimatedTime);
	    	    JOptionPane.showMessageDialog(null,"DELETE T�ME �S :"+estimatedTime+" nano seconds" );
	    	   	         	    	   	
	    	}
	    	}
		}
		);

   
        
        
        frame.setVisible(true);

}


     }
